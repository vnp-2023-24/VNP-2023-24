# VNP-2023-24



## Getting started

The file requirements.txt contains the required Python packages you need to install to be able to run the notebooks.
THE REQUIREMENTS WILL BE UPDATED AS WE LEARN MORE PACKAGES, MAKE SURE TO CHECK AND INSTALL ANYTHING THAT YOU'RE MISSING.
It's recommended to use Anaconda to manage the packages and to easily access Jupyter Notebook.!

## Optional - Create a Virtual Environment

Option 1

Create virtual environment - Anaconda interface
Create a virtual environment. Virtual environments help you separate the version of Python and the packages you use for each project. If you don't create a virtual environment, the package versions and the Python version are global for your entire system.

Install packages
In Anaconda you can create a virtual environment by clicking the '+' sign in the Environments tab. Give the environment a name, and search for the packages from the requirements.txt file to install them by clicking on the '+' sign.

Option 2
You can create an environment using Python's virtual environment module in the following way:

python3 -m venv <name_or_path_to_environment>
source venv/bin/activate
venv\Scripts\activate.bat
pip install -r requirements.txt
